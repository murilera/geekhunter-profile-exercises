"""Quantidade de Gols marcados.

O número de gols conquistado em partidas por duas equipes de futebol é dado em forma de duas listas.

Considere:
A equipe A jogou 4 partidas e marcou 0, 7, 5, 2 gols em cada partida respectivamente (equipeA = [0, 7, 5, 2]).
A equipe B jogou 3 partidas e marcou 6, 1 e 5 gols em cada partida respectivamente (equipeB = [6, 1, 5]).

Para cada partida da equipe B, calcule o número total de partidas em que a equipe A marcou menos ou a mesma quantidade de gols.

No caso abaixo:
Em sua primeira partida a Equipe B fez 6 gols. Neste caso a equipe A tem 3 partidas com 6 ou menos gols marcados, partidas [1, 3, 4]. O comprimento deste array é de 3 (partidas), portanto a resposta no momento é:
Em sua segunda partida, a equipe B marcou 1 gol. Neste caso a equipe A tem apenas uma partida com 1 ou menos gols, partida [1]. O comprimento deste array é de 1 (partidas) portanto a resposta agora é:
Em sua terceira partida a equipe B marcou 5 gols. Neste caso a equipe A tem 3 partidas com 5 ou menos gols [1, 3, 4]. O comprimento deste array é 3 (partidas) portanto a resposta agora é:
Resposta: [3, 1, 3]

A função deve imprimir o array m de números inteiros positivos, um elemento por linha. Para cada elemento equipeB[i] selecione a quantidade de elementos em equipeA onde, equipeA[j] ≤ equipeB[i], 0 ≤ j < n e 0 ≤ i < m, na ordem dada.

Restrições conhecidas:

2 ≤ n, m ≤ 105
1 ≤ equipeA[j] ≤ 109, onde 0 ≤ j < n.
1 ≤ equipeB[i] ≤ 109, onde 0 ≤ i < m."""

"Resposta:"

global a

def processInput ( input ):

    qnt_jogos_a = int(input[0])
    qnt_jogos_b = int(input[qnt_jogos_a+1])

    equipeA = []
    equipeB = []

    for i in range(1, qnt_jogos_a+1):
      equipeA.append(input[i])

    for j in range(1, qnt_jogos_b+1):
      equipeB.append(input[-j])

    equipeB = equipeB[::-1]

    for goalsB in equipeB:
      qnt_gols = 0
      for goalsA in equipeA:
        if goalsB >= goalsA:
          qnt_gols += 1
      yield qnt_gols

#Este e um exemplo de processamento de entradas (inputs), sinta-se a vontade para altera-lo conforme necessidade da questao.
value = input()
a = []
while (value):
    a.append(value)
    try:
        value = input()
    except EOFError:
        break

result = processInput(a)
for r in result:
  print(r)