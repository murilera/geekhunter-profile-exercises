"""Calcule os zeros

Dado uma entrada composta por dois números, conte quantos zeros existem entre estes dois números (incluindo eles mesmos).
Deve ser contado cada zero presente nos números.
Os números sempre são positivos e entre 0 e 999, e o primeiro número sempre é menor do que o segundo.
A primeira linha da entrada sempre o números de tuplas que serão passadas pelo algoritmo e as próximas linhas serão as tuplas (uma por linha).

Veja o exemplo abaixo:

Entrada:
5
17 18
199 200
199 301
400 500
0 102

Saída:
0
2
23
22
14
PS: O teste é automaticamente avaliado, similar a um teste unitário, o formato de saída deve ser idêntico ao esperado como mostrado no exemplo acima."""

"Resposta:"

def calculate_zeros(a, b):
  count_zeros = 0
  while(a <= b):
    for i in str(a):
      if "0" in i:
        count_zeros = count_zeros + 1
    a = a + 1
  return count_zeros

entradas = input()
value = input()
valores = []

while (value):
    valores.append(value.strip().split(" "))
    try:
        value = input()
    except EOFError:
        break

for v in valores:
  print(calculate_zeros(int(v[0]), int(v[1])))