"""Maior divisor comum
Nesta questão vamos implementar uma maneira simples de calcular o maior divisor comum (MDC) entre dois números.
Leia as instruções atentamente e NÃO USE funções de bibliotecas (como por exemplo __gcd() do C++).
Uma maneira simples de calcular o MDC de dois números positivos Ae Bconsiste em:
Identificar o menor número entre A e B, supondo que B< A.
Se B é igual a zero, então o MDC é igual a A.
Caso contrário o MDC entre A e B será o mesmo que o MDC de B e (A % B), onde (A % B) representa o resto de A quando dividido por B. Para calcular o MDC de B e (A % B) podemos aplicar o mesmo método.
Escreva um programa para calcular o MDC de dois números positivos A e B.

Entrada:
A primeira linha contém um inteiro N que informa o número de linhas subsequentes que virão. Cada uma das N linhas irá conter dois números A e B (1 =< *A*, *B* =< 1000).

Saída:
Cada linha que contém os dois números inteiros A e B, imprima o MDC de A e B."""

"Resultado:"

def mdc(n, m):
  mdc = n
  while n % mdc != 0 or m % mdc != 0:
    mdc = mdc - 1
  return f'{mdc}'

entradas = input()
value = input()

while (value):
    n_m = value.split(" ")
    print(mdc(int(n_m[0]), int(n_m[1])))
    try:
        value = input()
    except EOFError:
        break