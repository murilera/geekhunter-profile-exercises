"Soma de vetor com elementos unicos"

"Resposta:"

def process_(vetor):
  oc_set = set()
  res = []
  for idx, val in enumerate(vetor):
    if val not in oc_set:
        oc_set.add(val)
    else:
        res.append(idx)
  return res

def sum_element(vetor, duplicates):
  for i in duplicates:
    vetor[i] = int(vetor[i]) + 1

def sum_list(vetor):
    result = 0
    for i in vetor:
        result = result + i
    return result

input_ = ['1943', '2366', '969', '1400', '143', '1943', '502', '1902', '1299', '1178', '1746', '969', '1299', '1902', '1636', '2009', '2905', '502', '1969', '1178', '2829', '114', '2176', '2610', '1672', '1636', '1261', '138', '1178', '1636', '114', '2768', '2829', '969', '138', '1943', '1943', '1902', '2009', '2531']

vetor = []
for i in input_:
    vetor.append(int(i.strip()))

duplicates = process_(vetor)
while (duplicates):
    sum_element(vetor, duplicates)
    duplicates = process_(vetor)

print(sum_list(vetor))