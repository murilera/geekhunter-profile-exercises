"""FizzBuzz
Neste desafio, considere um intervalo de inteiros positivos de 1 até N. Para cada valor imprima ou uma string ou o valor baseado se o número é um múltiplo de 3, 5, ambos ou nenhum. Determine o valor a imprimir baseado nas seguintes regras:
Se o inteiro é múltiplo ambos de 3 e 5, imprima FizzBuzz.
Se o inteiro é múltiplo de 3 (mas não de 5), imprima Fizz.
Se o inteiro é múltiplo de 5 (mas não de 3), imprima Buzz.
Para todas as outras opções imprima o inteiro.


Entrada:
15
Será um número inteiro N onde 0 < N < 2x105. Ex:

Saída:
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz

Uma linha impressa para cada número inteiro entre 1 até N seguindo as regras do FizzBuzz acima. Ex:

Explicação:
Os números 3, 6, 9 e 12 são múltiplos de 3 (mas não de 5), então imprimimos Fizz nestas linhas.
Os números 5 e 10 são múltiplos de 5 (mas não de 3), então imprimimos Buzz nestas linhas.
O número 15 é um múltiplo de 3 e 5, então imprimimos FizzBuzz nesta linha.
Os outros números não são múltiplos nem de 3 ou 5, então imprimimos o próprio valor do inteiro nestas linhas."""

"Resposta:"

def fizzbuzz_check(value):
  for fizzbuzz in range(1, value+1):
    if fizzbuzz % 3 == 0 and fizzbuzz % 5 == 0:
        print("FizzBuzz")
        continue
    elif fizzbuzz % 3 == 0:
        print("Fizz")
        continue
    elif fizzbuzz % 5 == 0:
        print("Buzz")
        continue
    print(fizzbuzz)

#Este e um exemplo de processamento de entradas (inputs), sinta-se a vontade para altera-lo conforme necessidade da questao.
value = input()
while (value):
    fizzbuzz_check(int(value))
    try:
        value = input()
    except EOFError:
        break