"""Transposição de matriz

Transponha uma matriz seguindo o conceito:
Tomando por base uma matriz A de ordem m x n, teremos a sua matriz transposta indicada por A t, invertendo as posições de m e n, isto é, n x m.

Entrada:
A primeira linha de entrada contém dois números m e n (1 ≤ m, n ≤ 200) correspondendo respectivamente ao número de linhas e ao número de colunas da matriz.
É então seguido de m linhas com n números em cada coluna.

Saída:
A saída deve conter a matriz transposta."""

"Resposta:"

def format_input(data):
  matrix_lines = []
  for d in data.replace(" ", ""):
    matrix_lines.append(d)
  matrix.append(matrix_lines)

def matrix_transposta(m):
        transposta = []
        for j in range(len(m[0])):
                transposta.append([m[i][j] for i in range(len(m))])
        return transposta

#Este e um exemplo de processamento de entradas (inputs), sinta-se a vontade para altera-lo conforme necessidade da questao.
m_n = input()
lines = input()
matrix = []

while(lines):
    format_input(lines)
    try:
        lines = input()
    except EOFError:
        break

result = matrix_transposta(matrix)
for r in result:
  print(*r)